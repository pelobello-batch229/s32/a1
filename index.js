let http = require("http");


http.createServer(function(req,res){
// Ver 3.0 ans
		 	// GET METHOD 
		if(req.method == "GET"){

			if(req.url == "/"){

				res.writeHead(200,{"Content-Type": "text/plain"});
				res.end("Welcome to Booking System");

			}else if(req.url == "/profile"){

				res.writeHead(200,{"Content-Type": "text/plain"});
				res.end("Welcome to Your Profile");

			}else if(req.url == "/courses"){

				res.writeHead(200,{"Content-Type": "text/plain"});
				res.end("Here's our courses available");

			}else{

				res.writeHead(404, {"Content-Type" : "text/plain"});
				res.end("404: Page not found");
			}

		// POST METHOD 
		}else if(req.method == "POST"){

			if(req.url == "/addcourse"){

				res.writeHead(200,{"Content-Type": "text/plain"});
				res.end("Add courses to our resouces");

			}else{

				res.writeHead(404, {"Content-Type" : "text/plain"});
				res.end("404: Page not found");
			}


		// PUT METHOD 
		}else if(req.method == "PUT"){

			if(req.url == "/updatecourse"){
				res.writeHead(200,{"Content-Type": "text/plain"});
				res.end("Update a courses to our resouces");

			}else{
				res.writeHead(404, {"Content-Type" : "text/plain"});
				res.end("404: Page not found");
			}

		// DElETE METHOD
		}else if(req.method == "DELETE"){
			if(req.url == "/archivecourses"){

				res.writeHead(200,{"Content-Type": "text/plain"});
				res.end("Archive courses to our resouces");

			}else{
				res.writeHead(404, {"Content-Type" : "text/plain"});
				res.end("404: Page not found");
			}

		}else{
			res.writeHead(404, {"Content-Type" : "text/plain"});
				res.end("404: Page not found");
		}
//  Vers 2.0 ans
/*		
		// GET METHOD 
		if(req.url == "/"){		

			if(req.method == "GET"){

				res.writeHead(200,{"Content-Type": "text/plain"});
				res.end("Welcome to Booking System");
			}

		}else if(req.url == "/profile"){

				if(req.method == "GET"){

					res.writeHead(200,{"Content-Type": "text/plain"});
					res.end("Welcome to Your Profile");
				}

		}else if(req.url == "/courses"){

				if(req.method == "GET"){

					res.writeHead(200,{"Content-Type": "text/plain"});
					res.end("Here's our courses available");
				}
		// POST METHOD
		}else if(req.url == "/addcourse"){ 

			if(req.method == "POST"){

				res.writeHead(200,{"Content-Type": "text/plain"});
				res.end("Add courses to our resouces");
			}

		// PUT METHOD
		}else if(req.url == "/updatecourse"){	

			if(req.method == "PUT"){

				res.writeHead(200,{"Content-Type": "text/plain"});
				res.end("Update a courses to our resouces");
			}
		// DELETE METHOD
		}else if(req.url == "/archivecourses"){	

			if(req.method == "DELETE"){

				res.writeHead(200,{"Content-Type": "text/plain"});
				res.end("Archive courses to our resouces");
			}

		}else{
				res.writeHead(404, {"Content-Type" : "text/plain"});
				res.end("404: Page not found");

		}

	 	




// Ver 1.0 ans
		if(req.url == "/" && req.method == "GET"){
			res.writeHead(200,{"Content-Type": "text/plain"});
			res.end("Welcome to Booking System");

		}

		if(req.url == "/profile" && req.method == "GET"){
			res.writeHead(200,{"Content-Type": "text/plain"});
			res.end("Welcome to Your Profile");

		}

		if(req.url == "/courses" && req.method == "GET"){
			res.writeHead(200,{"Content-Type": "text/plain"});
			res.end("Here's our courses available");

		}

		if(req.url == "/addcourse" && req.method == "POST"){
			res.writeHead(200,{"Content-Type": "text/plain"});
			res.end("Add courses to our resouces");

		}

		if(req.url == "/updatecourse" && req.method == "PUT"){
			res.writeHead(200,{"Content-Type": "text/plain"});
			res.end("Update a courses to our resouces");

		}
		
		if(req.url == "/archivecourses" && req.method == "DELETE"){
			res.writeHead(200,{"Content-Type": "text/plain"});
			res.end("Archive courses to our resouces");

		}
*/


}).listen(4000);

console.log("System is now running at localhost 4000");